<?php

/** 
 * Arquivo responsável por mandar as informações do pedido feito do Woocommerce para Softronic
 * 
 * @author Mateus Macedo
 */

add_action('template_redirect', 'woo_custom_redirect_after_purchase');

function woo_custom_redirect_after_purchase()
{

    global $wp;

    if (is_checkout() && !empty($wp->query_vars['order-received'])) {

        $order = wc_get_order($wp->query_vars['order-received']);
        $items = $order->get_items();

        $itemList = array();

        foreach ($items as $item) {
            $product_id = $item->get_product_id();
            array_push($itemList, array('CodProduto' => intval(get_post_meta($product_id, '_softronic_codigo_field', true)), 'CodigoProprio' => 'N', 'Quantidade' => $item->get_quantity()));
        }

        $softronic = new Softronic();
        $productInventory = $softronic->orderInsert(array("PedidoCliente" => $order->get_id(), "Itens" => $itemList));
    }
}
