<?php
/*
* Plugin name: WP Softronic
* Description: Plugin para integração do Woocommerce com a Softronic
* Version: 1.0
* Author: Helpper Soluções Inteligentes
*/
defined('ABSPATH') or die('No script kiddies please!');
require dirname(__FILE__) . '/Softronic.php';
require dirname(__FILE__) . '/functions.php';
require dirname(__FILE__) . '/cartFunction.php';
require dirname(__FILE__) . '/orderFunction.php';
require dirname(__FILE__) . '/settings.php';
