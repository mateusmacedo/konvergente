# Konvergente - Plugin Softronic + Woocommerce

Plugin do wordpress que comunica o Woocommerce com o Softronic

## Installation

*Pré-requisito: Precisa ter instalado o wordpress e o plugin do woocommerce ativado.*

Colocar em um arquivo ZIP os arquivos PHP e adicioná-lo como plugin do wordpress

## Usage

Tem alguns vídeos de testes mostrando a validação do plugin criado

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)