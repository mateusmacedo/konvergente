<?php

/** 
 * Arquivo responsável pela criação do menu para listar os pedidos
 * 
 * @author Mateus Macedo
 */

// Nomes de constantes válidos
define("PAGE_TITLE",     "KONVERGENTE TITLE");
define("MENU_LABEL",    "KONVERGENTE MENU LABEL");
define("LIMIT_ORDER", 100);

/*
 *  Função para criar o menu na configuração do wordpress
 */
function theme_options_panel()
{
    add_menu_page(PAGE_TITLE, MENU_LABEL, 'manage_options', 'theme-options', 'wps_theme_func');
}
add_action('admin_menu', 'theme_options_panel');

function wps_theme_func()
{
    echo '<div class="wrap"><div id="icon-options-general" class="icon32"><br></div>
          <h2>LISTA DOS PEDIDOS</h2></div>';
    $args = array(
        'limit' => LIMIT_ORDER,
        'offset' => 0,
    );
    $orders = wc_get_orders($args);

    foreach ($orders as $order) {
        echo '<br>';
        echo '<strong>Número do Pedido: </strong>' . $order->get_id();
        echo '<br>';
        echo '<strong>Nome do pagador: </strong>' . $order->get_billing_first_name();
        echo '<br>';
        echo '<strong>Email do pagador: </strong>' . $order->get_billing_email();
        echo '<br>';
        echo '<strong>Telefone do pagador: </strong>' . $order->get_billing_phone();
        echo '<br>';
        echo '<strong>Valor do pedido: </strong>' . $order->get_total();
        echo '<br>';
        $items = $order->get_items();
        foreach ($items as $item) {
            $product_name = $item->get_name();
            $product_id = $item->get_product_id();
            echo '<strong>Nome do Produto: </strong>' . $product_name;
            echo '<br>';
            echo '<strong>Quantitade : </strong>' . $item->get_quantity();
            echo '<br>';
            $product = wc_get_product($product_id);
            echo '<strong>Valor do Produto : </strong>' . $product->get_price();
            echo '<br>';
        }
    }
}
