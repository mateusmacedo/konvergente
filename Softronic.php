<?php

/**
 * Softronic
 *
 * Classe padrão de comunicação com a Softronic
 *
 * @author Henrique de Castro
 */
class Softronic {

	// URL base
	private $__url = "http://webservices.softronic.com.br:8092/datasnap/rest/tsmdemo";

	// Parâmetros de autenticação
	private $__auth = array(
		"username" => "KONVERGENTE",
		"password" => "K8X3Y7A"
	);

	// Código do cliente
	private $__client = 6535;

	// Condição de Pagamento
	private $__payment_terms = 87;

	// Objeto do cURL
	private $__curl;

	// Debug?
	public $debug = false;

	/**
	 * constructor
	 *
	 * Classe padrão de comunicação com a Softronic
	 *
	 * @author Henrique de Castro
	 */
	public function __construct(){
		
		// Inicia o cURL
		$this->__curl = curl_init();

		// Adiciona a autenticação
		curl_setopt($this->__curl, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
		curl_setopt($this->__curl, CURLOPT_USERPWD, $this->__auth["username"] . ":" . $this->__auth["password"]);
		curl_setopt($this->__curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($this->__curl, CURLOPT_RETURNTRANSFER, true);

	}

	/**
	 * productList
	 *
	 * Lista os Produtos
	 *
	 * @author Henrique de Castro
	 * @param  Integer Código do Produto (Zero para todos)
	 * @param  Integer Limite Inicial
	 * @param  Integer Limite Final
	 * @return Objeto resultante da API
	 */
	public function productList($product = 0, $limit = false, $offset = false){

		// Monta os dados
		$data = array($product);
		if($limit)
			$data[] = $limit;
		if($offset)
			$data[] = $offset;
		
		// Executa a chamada
		return $this->__callAPI("ListaProduto", "GET", $data);
	}

	/**
	 * productInventory
	 *
	 * Lista o estoque dos Produtos
	 *
	 * @author Henrique de Castro
	 * @param  Integer Código do Produto (Zero para todos)
	 * @param  Integer Limite Inicial
	 * @param  Integer Limite Final
	 * @return Objeto resultante da API
	 */
	public function productInventory($product = 0, $limit = false, $offset = false){

		// Monta os dados
		$data = array($product);
		if($limit)
			$data[] = $limit;
		if($offset)
			$data[] = $offset;
		
		// Executa a chamada
		return $this->__callAPI("EstoquePreco", "GET", $data);
	}

	/**
	 * orderList
	 *
	 * Lista os Pedidos
	 *
	 * @author Henrique de Castro
	 * @param  Integer Código do Pedido (código da Softronic)
	 * @return Objeto resultante da API
	 */
	public function orderList($order){

		// Executa a chamada
		return $this->__callAPI("Pedidos", "GET", array($order));
	}

	/**
	 * orderItemList
	 *
	 * Lista os Itens dos Pedidos
	 *
	 * @author Henrique de Castro
	 * @param  Integer Código do Pedido (código da Softronic)
	 * @return Objeto resultante da API
	 */
	public function orderItemList($order){

		// Executa a chamada
		return $this->__callAPI("ItensPedido", "GET", array($order));
	}

	/**
	 * orderInsert
	 *
	 * Insere um pedido
	 *
	 * @author Henrique de Castro
	 * @param  Array Pedido e seus itens
	 * @return Objeto resultante da API
	 */
	public function orderInsert($items){
		
		// Monta os dados
		$data = array(
			"CodCliente" => $this->__client, "CondPagto" => $this->__payment_terms
		);
		$data = array_merge($data, $items);

		// Executa a chamada
		return $this->__callAPI("Pedidos", "PUT", $data);
	}

	/**
	 * __callAPI
	 *
	 * Função padrão para chamadas a API
	 *
	 * @author Henrique de Castro
	 * @param  String
	 * @param  String
	 * @param  Array
	 * @return Objeto resultante da API
	 */
	private function __callAPI($action, $method = "GET", $data = array()){

		// Trata os dados
		$url = $this->__url . "/" . $action;
		if($method == "PUT"){
			$data = json_encode($data);
			curl_setopt($this->__curl, CURLOPT_POST,           true);
			curl_setopt($this->__curl, CURLOPT_CUSTOMREQUEST,  $method);
			curl_setopt($this->__curl, CURLOPT_POSTFIELDS,     $data);
		}
		else {
			$url .= "/". $this->__client . "/". implode("/", $data);
		}

		// Adiciona a URL
		curl_setopt($this->__curl, CURLOPT_URL, $url);
		
		// Executa
		$return = curl_exec($this->__curl);
		
		// Debug?
		if($this->debug) {
			echo "<pre>";
			var_dump(curl_getinfo($this->__curl));
			echo "</pre>";
		}

		// Limpa a memória
		curl_close($this->__curl);

		// Retorna a execução
		return json_decode($return);
	}
}

?>