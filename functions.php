<?php
/*
 * Custom field com o código da Softronic
 */

require_once("Softronic.php");

$softronic = new Softronic();

function softronic_add_custom_fields()
{

	echo '<div class="options_group">';

	// Number Field
	woocommerce_wp_text_input(
		array(
			'id'                => '_softronic_codigo_field',
			'label'             => __('Código do Produto na Softronic', 'woocommerce'),
			'placeholder'       => '',
			'desc_tip'    		=> true,
			'description'       => __("Aqui devemos colocar o código do produto que iremos vender da Softronic", 'woocommerce'),
			'type'              => 'number',
			'custom_attributes' => array(
				'step' 	=> 'any',
				'min'	=> '0'
			)
		)
	);

	echo '</div>';
}
add_action('woocommerce_product_options_general_product_data', 'softronic_add_custom_fields');


/*
 * Hook para buscar o save
 */

function softronic_add_custom_fields_save($post_id)
{

	$woocommerce_softronic_codigo_field = $_POST['_softronic_codigo_field'];

	$softronic = new Softronic();
	$productList = $softronic->productList($woocommerce_softronic_codigo_field);

	if (!empty($productList)) {
		update_post_meta($post_id, '_softronic_codigo_field', esc_attr($woocommerce_softronic_codigo_field));
	} else {
		//Nao existe	
		$adminnotice = new WC_Admin_Notices();
		$adminnotice->add_custom_notice("O código do softronic não existe", "O código softronic não existe");
		$adminnotice->output_custom_notices();
	}
}
add_action('woocommerce_process_product_meta', 'softronic_add_custom_fields_save');
