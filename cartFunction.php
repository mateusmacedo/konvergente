<?php

/** 
 * Arquivo responsável pela comunicação do softronic com o carrinho de compras
 * 
 * @author Mateus Macedo
 */

require_once("Softronic.php");

// Nomes de constantes válidos
define("MESSAGE_WITHOUT_STOCK",     "Sem quantidade no estoque");
define("MESSAGE_QUANTITY_BIGGER",    "Não é possível adicionar mais desse item no carrinho, porque no estoque não possui mais");

/*
 * Função é chamada quando o usuário clica para adicionar no carrinho. Verifica se tem a quantidade informada no estoque da softronic. Se não tiver, aparece um erro
 */
function cart_stock_validation($passed_validation, $product_id, $quantity)
{

    $control = false;
    $quantityCart = 0;

    foreach (WC()->cart->get_cart() as $cart_item) {

        if ($product_id == $cart_item['product_id']) {
            $control = true;
            $quantityCart = $cart_item['quantity'];
            break;
        }
    }

    $product = wc_get_product($product_id);
    $sofTronicField = $product->get_meta('_softronic_codigo_field');

    $softronic = new Softronic();
    $productInventory = $softronic->productInventory($sofTronicField);

    if (sizeof($productInventory) > 0) {
        if ($productInventory[0]->estoque < $quantity || $productInventory[0]->estoque <= 0) {
            wc_add_notice(__(MESSAGE_WITHOUT_STOCK, 'woocommerce'), 'error');
            return false;
        }
    }

    if ($control == true) {
        if ($productInventory[0]->estoque < $quantity + $quantityCart) {
            wc_add_notice(__(MESSAGE_QUANTITY_BIGGER, 'woocommerce'), 'error');
            return false;
        }
    }
    return true;
}

add_filter('woocommerce_add_to_cart_validation', 'cart_stock_validation', 10, 3);


add_action('woocommerce_after_cart_item_quantity_update', 'limit_cart_item_quantity', 20, 4);

/*
 * Função é chamada após a atualização da quantidade de itens no carrinho. Se a quantidade for maior que estiver no estoque da softronic, aparecerá uma mensagem de alerta e a quantidade voltará para o valor anterior
 */

function limit_cart_item_quantity($cart_item_key, $quantity, $old_quantity, $cart)
{

    //wc_add_notice( __('Quantity limit reached for this item:'), 'notice' );

    $cart = WC()->cart;
    $cart_data = $cart->get_cart();
    $cart_item = $cart_data[$cart_item_key];

    $product_id = $cart_item['product_id'];

    $sofTronicField = get_post_meta($product_id, '_softronic_codigo_field', true);

    $softronic = new Softronic();
    $productInventory = $softronic->productInventory($sofTronicField);

    if (sizeof($productInventory) > 0) {
        if ($productInventory[0]->estoque < $quantity || $productInventory[0]->estoque <= 0) {
            wc_add_notice(__('Sem quantidade informada no estoque do produto : ' . $cart_item['data']->get_title(), 'woocommerce'), 'error');
            $cart->cart_contents[$cart_item_key]['quantity'] = $old_quantity;
        }
    }
}


/*
add_action( 'woocommerce_before_shop_loop_item', 'wc_shop_page_product_date', 2 );

function wc_shop_page_product_date() {
    global $product;
	$sofTronicField = $product->get_meta('_softronic_codigo_field');
	
    $softronic = new Softronic();
    $productInventory = $softronic->productInventory($sofTronicField);
	
	
    if (sizeof($productInventory) > 0) {
        if ($productInventory[0]->estoque <= 0) {
            //wc_add_notice(__(MESSAGE_WITHOUT_STOCK, 'woocommerce'), 'error');
           
		//print_r($product->get_id());
			/*
			$terms = array( 'exclude-from-search', 'exclude-from-catalog' ); // for hidden..
			wp_set_post_terms($product->get_id(), $terms, 'product_visibility', false );
	
			
			    $product->set_catalog_visibility( 'hidden' );
				$product->save();
			//echo "bbb";
    		//update_post_meta( $product->get_id(), '_visibility', 'hidden' );
        } else {
			
			 $product->set_catalog_visibility( 'visible' );
			$product->save();
				//$terms = array( 'include-from-search', 'include-from-catalog' ); // for hidden..
				//wp_set_post_terms($product->get_id(), $terms, 'product_visibility', true );
				
//update_post_meta($product->get_id(), 'visible', true); 
		
		}  
    }
}
*/



add_filter('loop_shop_per_page', 'new_loop_shop_per_page');

function new_loop_shop_per_page($cols)
{
    // $cols contains the current number of products per page based on the value stored on Options -> Reading
    // Return the number of products you wanna show per page.
    // Array of args (above) 
    $args = array(
        'visibility' => array('hidden', 'visible')
    );

    $products = wc_get_products($args);
    foreach ($products as $product) {

        $sofTronicField = $product->get_meta('_softronic_codigo_field');
        $softronic = new Softronic();
        $productInventory = $softronic->productInventory($sofTronicField);

        if (sizeof($productInventory) > 0) {

            if ($productInventory[0]->estoque <= 0) {
                //wc_add_notice(__(MESSAGE_WITHOUT_STOCK, 'woocommerce'), 'error');

                //print_r($product->get_id());
                /*
			$terms = array( 'exclude-from-search', 'exclude-from-catalog' ); // for hidden..
			wp_set_post_terms($product->get_id(), $terms, 'product_visibility', false );
			*/
                $product->set_catalog_visibility('hidden');
                $product->save();
                //echo "bbb";
                //update_post_meta( $product->get_id(), '_visibility', 'hidden' );
            } else {
                $product->set_catalog_visibility('visible');
                $product->save();
                //echo $product->get_catalog_visibility();

                //Set product visible in catalog:
                //$terms = 'exclude-from-search';
                //wp_set_object_terms( $product->get_id(), $terms, 'product_visibility' );


                //update_post_meta($product->get_id(), 'visible', true); 

            }
        }
    }
}
